from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd


#code by pythonjar, not me
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)


#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:\Windows/chromedriver.exe', chrome_options=chrome_options)

##########Parameters
nbr_pages=70
#open the webpage
url='https://forums.techguy.org/forums/tech-related-news.80/page-'

lss=[]
for k in range(1,nbr_pages+1):
    myurl = url + str(k)
    driver.get(myurl)
    anchors = driver.find_elements_by_class_name('structItem-title')
    anchors = [a.find_elements_by_tag_name('a') for a in anchors]
    for a in anchors:
        for e in a:
            lss.append(e.get_attribute('href'))    

print('+++++++++++++++++')
print(len(lss))
print('+++++++++++++++++')
texts = []
#url_base = 'https://forums.techguy.org'
for k in range(len(lss)):
    if 'threads' in lss[k]:
        driver.get(lss[k])
        anchors = driver.find_elements_by_class_name('bbWrapper')
        anchors = [a.text for a in anchors]
        if(len(anchors)>0):
            texts.append(anchors[0]) #Because we need just the question and not the possible responses
    if k % 100 == 0:
        print(k)
    if k == 1200: #just we need 1200 rows
        break
df= pd.DataFrame()
df['text'] = texts
df['classe'] = 'Tech_News'
df.to_csv('Tech_News.csv',index=False)
print("End")
exit()