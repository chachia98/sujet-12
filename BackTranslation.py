from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import time
import pandas as pd

#code by pythonjar, not me
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)

#specify the path to chromedriver.exe (download and save on your computer)
driver = webdriver.Chrome('C:\Windows/chromedriver.exe', chrome_options=chrome_options)

##################################### List of sentences to translate #########################################


df=pd.read_csv('final_medical_data.csv')
df1 = df[df['classe']=='eye diseases and disorders']
texts_in = list(df1.text.values)

#df=pd.read_csv('eye_fr.csv')
#texts_in = list(df.text.values)


texts_out=[] #translated sentences


############################################ Google Translate  ###################################################
url_1 = "https://translate.google.com/?hl=fr&sl="
url_2 = "&tl="
url_3 = "&op=translate"
 
#translation codes : "English": "en" , "Arabic": "ar" , "French": "fr" , 
lg1_code = 'en'
lg2_code = 'fr'

url = url_1 + lg1_code + url_2 + lg2_code + url_3

#open the webpage of Google Translation
driver.get(url)
time.sleep(2)
#target input text
input_text = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "textarea[aria-label='Texte source']")))

for txt in texts_in:
    #enter input text
    input_text.clear()
    input_text.send_keys(txt)

    time.sleep(2)

    outs = driver.find_elements_by_xpath('/html/body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div[1]/div[2]/div[3]/c-wiz[2]/div[6]/div/div[1]/span[1]')

    for out in outs:
        texts_out.append(out.text)

df_neo_fr = pd.DataFrame()
df_neo_fr['text'] = texts_out

df_neo_fr.to_csv('eye_fr.csv',index=False)

#Close the webpage
driver.quit()