+++ Description des fichiers : 
*** DL_Models_NLP.ipynb
J'ai créé ce Notebook en parallèle avec la préparation du présentation des modèles DL de classification de texte.
j'ai travaillé sur l'ensemble de données 'Sentiment Analysis on Movie Reviews' disponible sur ce lien : https://www.kaggle.com/c/sentiment-analysis-on-movie-reviews . 
Cela m'aidera dans la tâche de mise en œuvre du modèle avec nos données de projet.

*** Techguy_Scraping.py
Ce fichier contient l'algorithme de data scraping du site https://forums.techguy.org/. 
Cela m'a permis de créer 5 classes : Problèmes réseau, Problèmes matériels, Sécurité générale, Développement logiciel, Tech News.

*** Medical_data_processing.py
Ce Notebook m'a permis de traiter et de concaténer des données liées au domaine médical. le résultat est un dataframe de 2 colonnes : text et classe.

*** Enron_data_processing.py
Ce Notebook m'a permis de traiter les emails du sociéte 'Enron' (https://www.kaggle.com/wcukierski/enron-email-dataset). 
Il contient des fonctions de traitement des données, des techniques de word embedding, modéles de clustering, metrics d'évaluation de clustering, modéle de Topic modelling,etc...
Il m'a permis de créer un classe de 'meeting invitation'.

*** Data_augmentation.ipynb
Ce notebook contient 2 techniques de data augmentation : Back translation et synonym replacement
Malheureusement, Back translation n'est pas efficace car l'utilisation de l'API de google translator est limitée à 10000 caractéres par jour.